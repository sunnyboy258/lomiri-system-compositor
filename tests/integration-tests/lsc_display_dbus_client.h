/*
 * Copyright © 2016 Canonical Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Alexandros Frantzis <alexandros.frantzis@canonical.com>
 */

#ifndef LSC_TEST_LSC_DISPLAY_DBUS_CLIENT_H_
#define LSC_TEST_LSC_DISPLAY_DBUS_CLIENT_H_

#include "dbus_client.h"

namespace lsc
{
namespace test
{

class LscDisplayDBusClient : public DBusClient
{
public:
    LscDisplayDBusClient(std::string const& address);

    DBusAsyncReplyString request_introspection();
    DBusAsyncReplyVoid request_turn_on(std::string const& filter);
    DBusAsyncReplyVoid request_turn_off(std::string const& filter);
    DBusAsyncReply request_active_outputs_property();
    DBusAsyncReply request_all_properties();
    DBusAsyncReply request_invalid_method();

    DBusMessageHandle listen_for_properties_changed();

    char const* const lsc_display_interface = "com.lomiri.SystemCompositor.Display";
};

}
}

#endif
